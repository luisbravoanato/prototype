##latest
* Added logo to splash screen.

##0.0.17 (Sun Sep 06 2015)
* Startup fix

##0.0.16 (Sun Sep 06 2015)
* Added splash screen

##0.0.13 (Mon Aug 31 2015)
* Added the login for displaying the current song.

##0.0.11 (Mon Aug 31 2015)
* Added spanish language.

##0.0.7 (Sun Aug 30 2015)
* Added i18n in the settings section.

##0.0.6 (Sat Aug 29 2015)
* Added i18n in the client.
* Added logo and brand colors.
##0.0.5 (Sun Jul 26 2015)
* Added build system.
* Added the detail screen for albums and artists in the web client.
##0.0.0
* Initial release.